/**
 * 
 */
package dto;

/**
 * @author Jose
 *
 */
public class Persona {
	
	private String nom;
	private int edad;
	private String lugar_nac;
	
	
	public Persona() {
		super();
	}


	public Persona(String nom, int edad, String lugar_nac) {
		super();
		this.nom = nom;
		this.edad = edad;
		this.lugar_nac = lugar_nac;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}


	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}


	/**
	 * @return the lugar_nac
	 */
	public String getLugar_nac() {
		return lugar_nac;
	}


	/**
	 * @param lugar_nac the lugar_nac to set
	 */
	public void setLugar_nac(String lugar_nac) {
		this.lugar_nac = lugar_nac;
	}


	@Override
	public String toString() {
		return "Persona [nom=" + nom + ", edad=" + edad + ", lugar_nac=" + lugar_nac + "]";
	}
	
	
	
	

}
